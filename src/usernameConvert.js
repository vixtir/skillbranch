export default function usernameConvert(params){
  const re = new RegExp(/(@)?(https?:)?(\/\/)?(www\.)?([0-9a-z\-]*\.[0-9a-z\-]*\/)?(\@)?([a-zA-Z0-9\.\_]*)/);
  const result = params.match(re)
  console.log(result)
  return '@' + result[7]
}
