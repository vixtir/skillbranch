import express from 'express';
import cors from 'cors';
import usernameConvert from './usernameConvert';

const app = express();
app.use(cors());

function initial(word) {
  return word.slice(0, 1) + '.';
};

function haveNumber(str) {
  const expr = new RegExp(/\/|_|\d/);
  return expr.test(str);
}

function removeSpaces(val) {
  return val !== '';
}

function capitalize(word) {
  return word[0].toUpperCase() + word.slice(1).toLowerCase();
}

function myConvert(string) {
  let arr = [];
  if (string.length !== 0 && !haveNumber(string)) {
    arr = string.split(' ').filter(removeSpaces).map(capitalize);
  } else {
    return 'Invalid fullname';
  }

  switch (arr.length){
    case 1 :
      return arr[0];
      break;
    case 2:
      return arr[1] + ' ' + initial(arr[0]);
      break;
    case 3:
      return arr[2] + ' ' + initial(arr[0]) + ' ' + initial(arr[1]);
      break;
    default:
      return 'Invalid fullname';
  };
}

app.get('/task2A', function (req, res) {
  const sum = (+req.query.a || 0) + (+req.query.b || 0);
  res.send(sum.toString());
});

app.get('/task2B', function (req, res) {
  const fullname = myConvert(req.query.fullname);
  res.send(fullname);
});

app.get('/task2C', function (req, res) {
  const username = usernameConvert(req.query.username);
  res.send(username);
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
